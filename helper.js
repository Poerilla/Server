  /**
  This module contains useful functions to be used by the server
  */
 module.exports = {
    /**
    Generate a replay nonce uniqe to the client
    */
    generateReplayNonce: function (length) {
      // base64url = [A-Z] / [a-z] / [0-9] / "-" / "_"
      let replay_nonce = '';
      let base64url = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-";

      for (let i = 0; i < length; i++){
        replay_nonce += base64url.charAt(Math.floor(Math.random() * (base64url.length)))
      }

      return replay_nonce;
    },
    /*****************************************************************************************************
    This is the leaky bucket starter class,
    */
    Bucket: class {
      constructor (request){
      // This is all going in the bucket
      this.request_bucket = [];
      this.rate_limit = 10;
      }

      add_request(request){
        //initialize for loop variables
        let flag = false;
        let counter = 0

        for (;counter < this.request_bucket.length;) {
          // If the ip address is already in the bucket increment the value
          if(this.request_bucket[counter].ip_address == request.connection.remoteAddress){
              this.request_bucket[counter].number_of_request++;
              //update flag to show that the ip address is in the address log
              flag = true;
              console.log(this.request_bucket[counter])
              console.log('updated the bucket');

          }
          // uppdate index counter
          counter++;
        }
        // If the ip address is not in the log, add it to the log
        if(flag == false) {
          this.request_bucket.push({ip_address: request.connection.remoteAddress, number_of_request: 1})
          console.log('added a new request to the bucket')
          console.log(this.request_bucket[this.request_bucket.length-1]);
        }
      }

      // check if the current ip address has reached it's rate limit
      // this is a bit redundant - consider refactoring
      check_rate(request){
        let flag = false;
        let counter = 0

        for (;counter < this.request_bucket.length;) {

          if(this.request_bucket[counter].ip_address == request.connection.remoteAddress && this.request_bucket[counter].number_of_request >= this.rate_limit){
              //update flag to show that the ip address has gone over the limit
              flag = true;
              console.log('rate limit reached for this ip address: error him/her');

          }
          // uppdate index counter
          counter++;
        }
        if(flag == false) console.log('rate limit not exceeded');
        return flag;
      }

    },
    /*****************************************************************************************************
    A class for checking the existence of an account
    The userid's are stored in an array which will be used to lookup

    @param {userId} = the user JS object which sent as per the ACME protocol
    */
    check_account: function(userId) {

      const hash    = require('hash.js');
      const fs      = require('fs');
      user_accounts = require('./data/accounts.json');

      // This flag is true if the account
      let flag = true;

      console.log("checking the user data in the database");
      // genereate a unique id for the user
      let uid = hash.sha256().update(JSON.stringify(userId)).digest('hex')
      console.log('Unique userId is:' + uid);

      if (user_accounts.users.includes(uid)) {
        // File is already in the database
        console.log("already in database");

      } else {
        // File is not in the database
        console.log('not in database');
        user_accounts.users.push(uid)
        flag = false;
        // First create an orders object which will be populated all through
        // the authorization process
        let d = new Date();
        let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        ordersObject = {
          status: "invalid",
          expires: `${days[d.getDay()]}, ${d.getDate()+1} ${months[d.getMonth()]} ${d.getFullYear()} 12:00:00 ET`,
          identifiers: [
            {type: "email", value: "admin@test.com"},
            {type: "keybase", value: "uname"}
          ],
          notBefore:`${days[d.getDay()]}, ${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()} 12:00:00 ET`,
          notAfter:`${days[d.getDay()]}, ${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()+1} 12:00:00 ET`,
          authorizations: [

          ],
          finalize: "",
          certificate:""
        };
        console.log(ordersObject);
        userId.orders = ordersObject;
        // create a user file based on their id
        fs.writeFile('./data/accounts/' + uid + '.json', JSON.stringify(userId), (err) => {
          if (err) throw err;
          console.log('The account json has been created and updated with a new user!');
        });
      }

      console.log('updating accounts.json: ');
      fs.writeFile('./accounts.json', JSON.stringify(user_accounts, null,2),(err) => {
        if (err) throw err;
        console.log('The file has been saved and updated!');
        return flag;
      });


    },

    /***********************************************SPLUNK******************************************************
    ----A function for splunking
    Splunk logging for JavaScript allows you to configure event logging to HTTP Event Collector on a
    Splunk Enterprise instance or in Splunk Cloud from within your JavaScript applications.
    In addition, Splunk provides a stream for Bunyan, a third-party logging library for Node.js,
    to HTTP Event Collector.

    Note: Both Bunyan stream for HTTP Event Collector and Splunk logging for JavaScript are  in beta.
    @param {reqPath} = the path variable of the request object recieved by the server
    */
    splunker: function(reqPath) {
      console.log("----------------------------------------");
      let SplunkLogger   = require('splunk-logging').Logger;
      let config = {
          token: "f0984c9d-145e-41ec-8379-1620a69ab937",
      };
      // Create a new logger
      let Logger = new SplunkLogger(config);
      Logger.error = function(err, context) {
        // Handle errors here
        console.log("error", err, "context", context);
      };

      let loggerPayload = {
          // Message can be anything, it doesn't have to be an object
          message: {
              RequestPath: reqPath,
              timestamp: Math.floor(Date.now() / 1000)
          },
          // Metadata is optional
          metadata: {
              source: "http:nodeACME",
              sourcetype: "httpevent",
              index: "main",
              host: "test.com:8080"
          },
          // Severity is also optional
          severity: "info"
      };

      console.log(loggerPayload);
      Logger.send(loggerPayload, function(err, resp, body) {
        // If successful, body will be { text: 'Success', code: 0 }
        console.log("Splunker: ", body);
      });


    },

    /**
     * Download a user account data
     * @param {object} queryString: the user details used to search the database
     * 
     * Todo: /acme.download_certificate()
     */
    get_account: function (queryString) {
      const fs = require('fs');

      console.log(queryString);
        //return the account data here
      
    },

    /**
     * Ecrypt with some  key
     * @param {string} data: data to be encrypted
     * 
     * Todo: /acme.encrypt()
     * Todo: encrypt data with private key
     * Todo: test and verify
     */
    encrypt: function (data) {
      const AES = require('crypto-js/aes')
      let key = require('./data/cer')
      let encrypted_data = AES.encrypt(data, key)

      return encrypted_data;
    },
    /**
     * Create a token given some user data
     * 
     * @param userData: JSON object containing userdata: Error checking to be done
     * 
     * Todo: /create_token
     * Todo: Save the token into the user data json in data
     * 
     */
    create_token: function (userData){
      // write the token creation code here

    }
  }
