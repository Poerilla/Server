/**

Example of a "static server implemented with the express framework.

Start the app:
install express module using the package.json file with command:
>npm install

>npm start

TO TEST:
Use browser to view pages at http://localhost:8080/index.html.
*/
const express         = require('express');
const app             = express();
const path            = require("path");
const parse           = require('url-parse'); //parse url's easy
const cookieParser    = require('cookie-parser');
const bodyParser      = require('body-parser');
const PORT            = process.env.PORT || 8080
const ROOT_DIR        = '/views'; //root directory for our static pages
const jws             = require('jws')
const myos            =require('os');


// helpMe is the helper class for the acme, it contains classes and functions
// used by the acme
let acme = require('./helper.js');
let leaky_bucket = new acme.Bucket();

/**
 * Our setup functions that run once and only once on start-up
 */
(function(){
  //use morgan logger to keep request log files
  // app.use( logger.createLogger({name: "nodeACME"}));
  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: true }))
  // parse application/json
  app.use(bodyParser.json())
  //set the default view engine, html files wil be rendered when res.render is called
  app.set('view engine', 'html');
  app.engine('html', require('hbs').__express);
  // app.use(cookieParser());
  console.log("done congifuring express");


}());

/**
 * The gate keeper: All requests come through here first
 * 
 * @function acme.splunker(): Log all requests to splunk server at port 8000
 * @class leaky_bucket: Data structure to for traffic policing and could initiate rate limiting procedures based on IP addresses
 * 
 * Todo: Fix cookie parsing or some sort of session management
 */
app.use(function(req, res, next){
//*******************************Logging****************************************
  acme.splunker(req.path);
  console.log("query string: ", parse(req.path));
  
//******************************************************************************
//*******************************Leaky Bucket **********************************
  //If the rate limit is reached for this ip address return the rate limit page
  if(leaky_bucket.check_rate(req) == true){
    res.redirect('rateLimited');
  };

  next(); //allow next route or middleware to run
});


/**
 * Server rate limited page
 */
app.get('/rateLimited', function(req,res){
  res.sendFile(path.join(__dirname+ROOT_DIR+'/rateLimited.html'))
})


/**
 * Main server page for online user interaction
 * 
 * To date there is no real use for this page but in the future clients should be able to 
 * register secure email using this web page. Although the cryptographic challenge which proves the existance
 * of a private key will still require the client to use the client Bot.
 */
app.get('/', function(req,res){
  res.sendFile(path.join(__dirname+ROOT_DIR+'/index.html'))
  console.log('sending out the index');
})

//Sample function for generating and assigning a replay-nonce
/**
 * Click-Replay: Nonce generator
 * 
 * @function acme.generateReplayNonce(): generate a 16 bit PseudoRandom number - base64url encoding
 * @param jwsObject: Crypto signature containing the generated nonce sent in the response head.
 */
app.all('/new-nonce', function(req,res){

  let replay_nonce =  acme.generateReplayNonce(16);
  console.log(replay_nonce)
  url = req.url

  let jwsObject = jws.sign({
    header: {alg: 'HS256' },
    payload: replay_nonce,
    secret: 'diffieup',
  });

  res.set({
    'Content-Type'  : 'text/plain',
    'Replay-Nonce'  : JSON.stringify(jwsObject),
    'Cache-Control' : 'no-strore'
  })

  res.send('This page has been intentianally left blank, check your head for more information');
})

/**Account Creation
 * url: /new-account - POST
 */
app.post('/new-account', function(req,res){

 // create the account
 res.status(200);
 res.send('send back an account url or something of the kind depending on your desired functionality')
});

/**
 * Operator: Send out an html or json object showing all resources available on the server
 * 
 */
app.get('/directory', function(req,res){
  res.set('Content-Type', 'application/json');
  res.send(JSON.stringify(require('./data/directory.json')))
})


/*
  Account Key Roll-over
*/
app.post('key-change', function(req, res){
  res.set('Content-Type', "application/json");
  //..........do something to do with key roll-ver here
  res.send("changing key");

})


app.use('/test-certificate-creation', function(req,res,next){
  acme.create_token({email: "nodeacme@gmail.com", username: "poe613"});
  res.set("Content-Type","application/json")
  res.send({message: 'this is a test-certificate-creation'})
})
/**
 * Download individual certificate
 * 
 * Todo: /download-certificate
 * Todo: Add query string so anyones certificate can be downloaded
 */
app.get('/download-account-details', function(req,res){
  let queryString = "black";

  if (req.query){
    queryString = req.query;
  }
  let account = acme.get_account(queryString)
  res.send(JSON.stringify(account));

})

app.listen(PORT, err => {
  if(err) console.log(err)
  else {console.log(`Server listening on port: ${PORT}`)}
  /******The following is just system information*******/
  //Endianess
  console.log('endianness       : ', myos.endianness());
  //OS
  console.log('type             : ', myos.type());
  //The OS platform
  console.log('The platform     : ', myos.platform());
  //The total system memory
  console.log('The total memory : ', myos.totalmem() + ' bytes.');
  //Total free memory
  console.log('The free memory  : ', myos.freemem() + ' bytes');
  //Hostname
  console.log('The hostname     : ', myos.hostname());
})

module.exports = app; // for testing
