// You must have noticed the we set the NODE_ENV variable to test, by doing so we change the 
// configuration file to be loaded so the server will connect to the test database and avoid 
// morgan logs in the cmd
process.env.NODE_ENV = 'test';

var assert = require('assert');
var fs = require('fs');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../nodeACME');
let should = chai.should();

chai.use(chaiHttp);

after("Done with the tests", ()=> {
    //server.close();
})

describe('Array', function () {

    describe('#indexOf()', function () {
        it('should return -1 when the value is not present', function () {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });

});

describe('fsRead', function () {
    describe('#readToken()', function () {
        it('should read from file without error', function (done) {
            fs.readFile('./data/client_secret.json', done)

        });
    });
});

describe('/GET directory', () => {
    it('it should GET all the server resources as a json', (done) => {
        chai.request(server)
            .get('/directory')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.length.should.be.eql(0);
                done();
            });
    });
});

describe('/GET new-Nonce', () => {
    it('it should GET all the server resources as a json', (done) => {
        chai.request(server)
            .get('/new-Nonce')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
                done();
            });
    });
});

describe('/GET download-certificate', () => {
    it('it should GET all the server resources as a json', (done) => {
        chai.request(server)
            .get('/download-certificate')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
                done();
            });
    });
});

describe('/GET /order-finalize', () => {
    it('it should GET a 200 OK response', (done) => {
        chai.request(server)
            .get('/order-finalize')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
                done();
            });
    });
});

describe('/GET /order', () => {
    it('it should GET a 200 OK response and JWS object showing the order details', (done) => {
        chai.request(server)
            .get('/order')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('json');
                res.body.length.should.be.eql(0);
                done();
            });
    });
});

describe('/GET /order-authorizations', () => {
    it('it should GET a 200 OK response', (done) => {
        chai.request(server)
            .get('/order-authorizations')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('string');
                res.body.length.should.be.eql(0);
                done();
            });
    });
});

describe('/GET /new-account', () => {
    it('it should GET a string link to resource and OK response', (done) => {
        chai.request(server)
            .get('/new-account')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('string');
                res.body.length.should.be.eql(0);
                done();
            });
    });
});

describe('/GET /', () => {
    it('it should GET a 200 OK response and index.html', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('html');
                res.body.length.should.be.eql(0);
                done();
            });
    });
});